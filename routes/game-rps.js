var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('game-rps', { title: 'CH4 : Game | Rock Paper Scissors' });
});

module.exports = router;
