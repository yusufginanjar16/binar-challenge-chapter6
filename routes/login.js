var express = require('express');
var router = express.Router();
var db = require('../database/user.json');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('login', { title: 'Chapter 5 | Login', errorMessage : '' });
});

router.post('/', function(req, res, next) {
    var loginSuccess = false;  // init value for loginSuccess is false
    for (let i = 0; i < db.length; i++) {
        if((req.body.username === db[i].username) && (req.body.password === db[i].password)){
            loginSuccess = true; // Set loginSuccess = true if user input correct username & password
        }
    }

    if(loginSuccess === true){
        res.status(200);
         res.redirect('/dashboard');

    } else {
        res.status(401);
        res.render('login', { title: 'Login', errorMessage : 'Wrong username or password' });
    }
});



module.exports = router;
